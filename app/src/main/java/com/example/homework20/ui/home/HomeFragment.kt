package com.example.homework20.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.homework20.BaseFragment
import com.example.homework20.R
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : BaseFragment() {
    private lateinit var homeViewModel: HomeViewModel

    override fun getLayoutResource() = R.layout.fragment_home
    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?

    ) {
        init()
    }

    private fun init() {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        homeViewModel.count.observe(viewLifecycleOwner, Observer {
            countTextView.text = it.toString()
        })
        rootView!!.increaseButton.setOnClickListener {
            homeViewModel.increase()
        }

    }
}
