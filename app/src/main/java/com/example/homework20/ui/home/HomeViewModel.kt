package com.example.homework20.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text

    private val _count = MutableLiveData<Int>().apply {
        value = 0
    }
    val count: LiveData<Int> = _count

//    private val count: MutableLiveData<Int> by lazy{
//        MutableLiveData<Int>()
//    }
//    fun initCount(){
//        count.value = 0
//    }
    fun increase() {
        _count.value = _count.value!!.plus(1)
    }
//    fun jemali(): MutableLiveData<Int>{
//        return count
//    }
}